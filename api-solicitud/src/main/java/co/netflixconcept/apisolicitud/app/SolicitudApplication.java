package co.netflixconcept.apisolicitud.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * SolicitudApplication
 *
 *
 * @author Juan.Herrera
 * @since --/--/----
 *
 *
 * Historia de Modificaciones
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ChrisV_G          18/07/2018     Se renombra la clase para tener una convención estandar
 *
 */

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@ComponentScan("co.netflixconcept.apisolicitud")
@EnableDiscoveryClient
@EnableMongoRepositories(basePackages = "co.netflixconcept.apisolicitud.repository")
public class SolicitudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolicitudApplication.class, args);
	}
}
