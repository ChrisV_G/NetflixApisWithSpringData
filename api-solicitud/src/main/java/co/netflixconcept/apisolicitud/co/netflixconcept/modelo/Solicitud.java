package co.netflixconcept.apisolicitud.co.netflixconcept.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(
        collection = "solicitud"
)
public class Solicitud implements Serializable {
    @Id
    private String id;
    private String codigoInstancia;
    private String codigoCiudadRadicacion;
    private String codigoDepartamentoRadicacion;

    public Solicitud() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigoInstancia() {
        return this.codigoInstancia;
    }

    public void setCodigoInstancia(String codigoInstancia) {
        this.codigoInstancia = codigoInstancia;
    }

    public String getCodigoCiudadRadicacion() {
        return this.codigoCiudadRadicacion;
    }

    public void setCodigoCiudadRadicacion(String codigoCiudadRadicacion) {
        this.codigoCiudadRadicacion = codigoCiudadRadicacion;
    }

    public String getCodigoDepartamentoRadicacion() {
        return this.codigoDepartamentoRadicacion;
    }

    public void setCodigoDepartamentoRadicacion(String codigoDepartamentoRadicacion) {
        this.codigoDepartamentoRadicacion = codigoDepartamentoRadicacion;
    }

    public String toString() {
        return "Solicitud{id='" + this.id + '\'' + ", codigoInstancia='" + this.codigoInstancia + '\'' + ", codigoCiudadRadicacion='" + this.codigoCiudadRadicacion + '\'' + ", codigoDepartamentoRadicacion='" + this.codigoDepartamentoRadicacion + '\'' + '}';
    }
}