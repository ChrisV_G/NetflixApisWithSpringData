/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.netflixconcept.apisolicitud.repository;

import co.netflixconcept.apisolicitud.co.netflixconcept.modelo.Solicitud;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * SolicitudRepository
 *
 *
 * @author ChrisV_G
 * @since --/--/----
 *
 *
 * Historia de Modificaciones
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 *
 *
 */
@Repository
public interface SolicitudRepository extends MongoRepository<Solicitud, String>{

    public Solicitud findByCodigoInstancia(String codigoInstancia);

}
