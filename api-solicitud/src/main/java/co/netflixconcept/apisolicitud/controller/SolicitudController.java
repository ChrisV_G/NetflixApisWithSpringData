
package co.netflixconcept.apisolicitud.controller;

import co.netflixconcept.apisolicitud.co.netflixconcept.modelo.Solicitud;
import co.netflixconcept.apisolicitud.repository.SolicitudRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * SolicitudController *
 *
 * @author ChrisV_G
 * @since --/--/----
 * <p>
 * <p>
 * Historia de Modificaciones
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ChrisV_G          18/07/2018     Se renombra la clase para tener una convención estandar
 * ChrisV_G          18/07/2018     Se cambia la forma en la que se crean los servicios REST para
 *                                  mayor facilidad con los componentes angular
 */
@RestController
@RequestMapping("/solicitudes")
public class SolicitudController {

    private static final Logger logger = LoggerFactory.getLogger(SolicitudController.class);


    @Autowired
    private SolicitudRepository solicitudRepository;

    /** Metodo que consulta todas las solicitudes en la base de datos
     *
     * @return Solicitudes almacenadas
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> findAll() {
        try {
            logger.info("Consultando solicitudes");
            return ResponseEntity.ok(solicitudRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    /** Buscar una solicitud por ID
     *
     * @param id de la solicitud
     * @return Solicitud almacenada
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> findOne(@PathVariable String id) {
        try {
            logger.info("Consultando solicitud con id -> "+id);
            return ResponseEntity.ok(solicitudRepository.findById(id).get());
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    /** Crea una solicitud nueva o actualiza una solicitud en base de datos
     *
     *
     * @param solicitud a guardar
     * @return Solicitud que se almacenó
     */
    @RequestMapping(value = "", method = { RequestMethod.PUT, RequestMethod.POST })
    @ResponseBody
    public ResponseEntity<?> createOrUpdate(@RequestBody Solicitud solicitud) {
        System.out.println(solicitud);
        try {
            logger.info("Almacenando solicitud -> " + solicitud);
            return ResponseEntity.ok(solicitudRepository.save(solicitud));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    /** Elimina una solicitud de la base de datos dado un ID
     *
     * @param id de la solicitud a eliminar
     * @return id de la solicitud eliminada
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable(value = "id") String id) {
        try {
            solicitudRepository.delete(solicitudRepository.findById(id).orElse(null));
            logger.info("eliminando solicitud -> " +id );
            return ResponseEntity.ok(id);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

}
