package co.netflixconcept.solicitud;


import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.netflixconcept.apisolicitud.app.SolicitudApplication;
import co.netflixconcept.apisolicitud.co.netflixconcept.modelo.Solicitud;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.Matchers.is;
import org.springframework.test.web.servlet.MvcResult;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes = SolicitudApplication.class)
@ActiveProfiles("test")
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class SolicitudApplicationTests {

    private MockMvc mockMvc;
    ObjectMapper mapper ;


    private final String ID_SOLICITUD="5b4f9d5bbc2cdb2a641b9b68";
    private final String CODIGO_SOLICITUD="1887";
    private final String CODIGO_CIUDAD="17001";
    private final String CODIGO_DEPARTAMENTO="12";
    private final String CODIGO_DEPARTAMENTO_TO_MODIFY="13";
    private static String ID_TO_DELETE;
    private Solicitud solicitud;


    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.solicitud =new Solicitud();
        this.solicitud.setCodigoCiudadRadicacion("99999");
        this.solicitud.setCodigoDepartamentoRadicacion("160012");
        this.solicitud.setCodigoInstancia("0012");
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.mapper= new ObjectMapper();
    }


    @Test
    public void testGetAllSolicitudes() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/solicitudes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3))).andDo(print());

    }




    @Test
    public void testCreateSolicitud() throws Exception {
        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.post("/solicitudes")
                .content(mapper.writeValueAsString(solicitud))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.codigoInstancia", is(solicitud.getCodigoInstancia())))
                .andExpect(jsonPath("$.codigoCiudadRadicacion", is(solicitud.getCodigoCiudadRadicacion())))
                .andExpect(jsonPath("$.codigoDepartamentoRadicacion", is(solicitud.getCodigoDepartamentoRadicacion()))).andDo(print())
                .andReturn();
        this.ID_TO_DELETE= this.mapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(),Solicitud.class).getId();
        System.out.println(solicitud.toString());


    }

    @Test
    public void testUpdateSolicitud() throws Exception {
        solicitud.setId("5b4f9949bc2cdb2ad425aeed");
        solicitud.setCodigoDepartamentoRadicacion(CODIGO_DEPARTAMENTO_TO_MODIFY);
        mockMvc.perform(MockMvcRequestBuilders.put("/solicitudes")
                .content(mapper.writeValueAsString(solicitud))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.codigoInstancia", is(solicitud.getCodigoInstancia())))
                .andExpect(jsonPath("$.codigoCiudadRadicacion", is(solicitud.getCodigoCiudadRadicacion())))
                .andExpect(jsonPath("$.codigoDepartamentoRadicacion", is(CODIGO_DEPARTAMENTO_TO_MODIFY))).andDo(print());
    }

    @Test
    public void testGetById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/solicitudes/{id}",ID_SOLICITUD))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(ID_SOLICITUD)))
                .andExpect(jsonPath("$.codigoInstancia", is(CODIGO_SOLICITUD)))
                .andExpect(jsonPath("$.codigoCiudadRadicacion", is(CODIGO_CIUDAD)))
                .andExpect(jsonPath("$.codigoDepartamentoRadicacion", is(CODIGO_DEPARTAMENTO))).andDo(print());
    }

    @Test
    public void testDeleteById() throws Exception {
        System.out.println(this.ID_TO_DELETE);
        mockMvc.perform(MockMvcRequestBuilders.delete("/solicitudes/{id}",ID_TO_DELETE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print());
    }

}
