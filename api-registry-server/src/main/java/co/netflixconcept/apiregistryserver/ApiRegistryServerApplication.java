package co.netflixconcept.apiregistryserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
/**
 *
 * ApiGatewayApplication
 *
 *
 * @author ChrisV_G
 * @since 17/07/2018
 *
 *
 * Historia de Modificaciones
 * --------------------------------------------------
 * Autor             Fecha          Modificacion
 * ----------------- -------------- ------------------
 */
@SpringBootApplication
@EnableEurekaServer
public class ApiRegistryServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRegistryServerApplication.class, args);
	}
}
