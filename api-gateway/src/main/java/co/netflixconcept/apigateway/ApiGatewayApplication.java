package co.netflixconcept.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 *
 * ApiGatewayApplication
 *
 *
 * @author ChrisV_G
 * @since 17/07/2018
 *
 *
 * Historia de Modificaciones
 * --------------------------------------------------
 * Autor             Fech|a          Modificacion
 * ----------------- -------------- ------------------
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class ApiGatewayApplication {

    public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
}
