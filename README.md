# NetflixApisWithSpringData

Repositorio que tiene prueba de concepto utilizando Spring Zuul para el api gateway, Eureka para el service discovery utilizando Spring boot y Spring data con MongoDB.
Tanto los servicios Solicitud como ServiceSaludar, se autoregistrarán en el eureka y se pueden consumir a través del ApiGateway
También contiene pruebas unitarias dirigidas a probar los servicios rest utilizando la librería Mockito

# Ejecución
Para correr los aplicativos se debe ejecutar el comando
```sh
$ gradlew bootRun  
```

# Tecnologías

Tecnilogías aplicadas:

* [Gradle] - Herramienta de construcción de empaquetados y manejo de dependencias
* [Spring boot] - Creación de aplicativos stand-alone
* [Spring data] - Manejo de la capa de persistencia
* [Zuul] - Librería de Netflix que se encarga del balanceo y redireccionamiento de peticiones
* [Eureka] - Librería de Netflix que se encarga del registro de servicios

# Sugerencias 

### Consumo de servicios

Para el ejemplo se deben consumir los servicios a través del ApiGatewa el cuál a su vez hace de balanceador de cargas (subir más de una instancia ya sea del servicio Solicitudes o Saludar para notarlo)
```
http://[[dirección-ip-del-host]]:[[puerto (por defecto 7100)]]/solicitudes
```

### Base de datos MongoDB

Si se desea levantar una base de datos mongo en docker se recomienda usar estos comandos:
```sh
docker pull mongo
docker run --name my_mongo -d -p 27017:27017 mongo
``` 

### Agregar el parámetro de JVM para el correr el perfil DEV

Al agregar las pruebas unitarias también se agregó la perfilación para que al momento de ejetucar los UT no se tome en cuenta el descubrimiento del Eureka y haga las pruebas de base de datos en un esquema (colección) de pruebas, para esto el UT reconoce automáticamente el perfil de pruebas pero para la ejecución normal se debe agregar el siguiente parámetro a través de la JVM
```sh
-Dspring.profiles.active=dev
```

